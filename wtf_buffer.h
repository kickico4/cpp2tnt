#ifndef WTF_BUFFER_H
#define WTF_BUFFER_H

#include <vector>
#include <functional>

/// lazy buffer (my bad :)
struct read_buffer;

class wtf_buffer
{
    friend read_buffer;
public:
    explicit wtf_buffer(size_t size = 1024 * 1024);
    ~wtf_buffer();
    size_t capacity() const noexcept;
    size_t size() const noexcept;
    char* data() noexcept;
    const char* data() const noexcept;
    void reserve(size_t size);
    void resize(size_t size);
    void clear() noexcept;
    void swap(wtf_buffer &other) noexcept;
//    char *end; // let msgpuck to write directly
    wtf_buffer(wtf_buffer &&);
    wtf_buffer& operator= (wtf_buffer &&);
    wtf_buffer(const wtf_buffer &) = delete;
    wtf_buffer& operator= (const wtf_buffer &) = delete;

    //make rest size at least count
    void requireRest(size_t count);

    void mp_encode_bool(bool val);
    void mp_encode_nil();
    void mp_encode_uint(uint64_t val);
    void mp_encode_int(int64_t val);
    void mp_encode_map(size_t count);
    void mp_encode_array(size_t count);
    void append(const char* data, size_t len);
    void mp_encode_strl(const char* data, size_t len);

    std::function<void()> on_clear;

    char *end{nullptr}; //unsafe!


    char *begin{nullptr};
//private:
    size_t rest{0}; //made public for testing purposes
};

struct read_buffer {
    read_buffer() {}
    read_buffer(wtf_buffer &&w) {
        begin = w.begin;
        end = w.end;
        auto usedSize = end - begin;
        auto newBegin = (char*)realloc((void*)begin, usedSize);
        if(!newBegin) {
            throw std::bad_alloc();
        } else {
            begin = newBegin;
            end = begin + usedSize;
        }
        w.begin = nullptr;
        w.end = nullptr;
        w.rest = 0;
    }
    ~read_buffer() {
        if(begin) {
            free(begin);
        }
    }
    read_buffer(read_buffer &) = delete;
    read_buffer(read_buffer &&) = delete;
    read_buffer &operator=(const read_buffer &) = delete;
    read_buffer &operator=(const read_buffer &&) = delete;

    size_t size() const noexcept {
        return end - begin;
    }

    char *begin = 0;
    char *end = 0;
};

#endif // WTF_BUFFER_H
