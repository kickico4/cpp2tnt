//
// Created by ivan2kh on 3/4/20.
//

#define CATCH_CONFIG_RUNNER
#include "catch2/catch.hpp"

#include "catch2/catch.hpp"

#include "wtf_buffer.h"

TEST_CASE( "wtf_buffer realloc test", "[wtf_buffer]" ) {
    wtf_buffer w(0);
    REQUIRE(w.capacity() == 0);
    w.reserve(5);
    REQUIRE(w.capacity() == 5);
    w.reserve(11);
    REQUIRE(w.capacity() == 11);
    w.reserve(16);
    REQUIRE(w.capacity() == 16);
    w.reserve(51);
    REQUIRE(w.capacity() == 51);

    {
        wtf_buffer w(20);
        w.mp_encode_array(2);
        REQUIRE(w.size() + w.rest == 20);
    }
    {
        wtf_buffer w(20);
        w.mp_encode_uint(2);
        REQUIRE(w.size() + w.rest == 20);
    }
    {
        wtf_buffer w(20);
        w.mp_encode_strl("sndcjk", 4);
        REQUIRE(w.size() + w.rest == 20);
    }
    {
        wtf_buffer w(20);
        w.mp_encode_map(2);
        REQUIRE(w.size() + w.rest == 20);
    }

    {
        wtf_buffer w(20);
        w.mp_encode_bool(1);
        REQUIRE(w.size() + w.rest == 20);
    }
    {
        wtf_buffer w(20);
        w.mp_encode_nil();
        REQUIRE(w.size() + w.rest == 20);
    }
    {
        wtf_buffer w(20);
        w.mp_encode_int(-2);
        REQUIRE(w.size() + w.rest == 20);
    }
    {
        wtf_buffer w(20);
        w.append("123123", 4);
        REQUIRE(w.size() + w.rest == 20);
    }
}

int main() {
    int result = Catch::Session().run();//.run( argc, argv );
    return result;
}