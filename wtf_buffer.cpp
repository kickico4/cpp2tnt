#include "msgpuck/msgpuck.h"
#include "wtf_buffer.h"
#include <iostream>

wtf_buffer::wtf_buffer(size_t size) {
    begin = (char *) malloc(size);
    end = begin;
    rest = size;
}

size_t wtf_buffer::capacity() const noexcept {
    return end - begin + rest;
}

size_t wtf_buffer::size() const noexcept {
    return end - begin;
}

char *wtf_buffer::data() noexcept {
    return begin;
}

const char *wtf_buffer::data() const noexcept {
    return begin;
}

void wtf_buffer::reserve(size_t newSize) {
    size_t prevSize = size();
    if (newSize <= prevSize + rest) {
        return;
    }
    char *newBegin = (char *) realloc((void *) begin, newSize);
    if(!newBegin) {
        throw std::bad_alloc();
    }
//    std::cout<<"reserved " << newSize << " " << (void*)newBegin << "\n";
    begin = newBegin;
    end = begin + std::min(prevSize, newSize);
    rest = begin + newSize - end;
}

void wtf_buffer::requireRest(size_t count) {
    if (rest < count) {
        reserve(size() + count);
    }
}

void wtf_buffer::resize(size_t newSize) {
    if (newSize > capacity()) {
        reserve(newSize);
    }
    int addendum = newSize - size();
    rest -= addendum;
    end += addendum;
}

void wtf_buffer::clear() noexcept {
    rest += size();
    end = begin;
    if (on_clear)
        on_clear();
}

void wtf_buffer::swap(wtf_buffer &other) noexcept {
    if (this == &other) {
        return;
    }
    std::swap(begin, other.begin);
    std::swap(end, other.end);
    std::swap(rest, other.rest);
}

wtf_buffer::wtf_buffer(wtf_buffer &&src) {
    swap(src);
}

wtf_buffer &wtf_buffer::operator=(wtf_buffer &&src) {
    swap(src);
    return *this;
}

void wtf_buffer::mp_encode_bool(bool val) {
    requireRest(2);

    auto newEnd = ::mp_encode_bool(end, val);

    rest -= newEnd - end;
    end = newEnd;
}

void wtf_buffer::mp_encode_nil() {
    requireRest(2);

    auto newEnd = ::mp_encode_nil(end);

    rest -= newEnd - end;
    end = newEnd;
}

void wtf_buffer::mp_encode_uint(uint64_t val) {
    requireRest(10);

    auto newEnd = ::mp_encode_uint(end, val);

    rest -= newEnd - end;
    end = newEnd;
}

void wtf_buffer::mp_encode_int(int64_t val) {
    requireRest(10);

    auto newEnd = ::mp_encode_int(end, val);

    rest -= newEnd - end;
    end = newEnd;
}

void wtf_buffer::mp_encode_map(size_t count) {
    requireRest(6);

    auto newEnd = ::mp_encode_map(end, count);

    rest -= newEnd - end;
    end = newEnd;
}

void wtf_buffer::mp_encode_array(size_t count) {
    requireRest(6);

    auto newEnd = ::mp_encode_array(end, count);

    rest -= newEnd - end;
    end = newEnd;
}

void wtf_buffer::append(const char *data, size_t len) {
    requireRest(len);
    memcpy(end, data, len);
    rest-=len;
    end+=len;
}

void wtf_buffer::mp_encode_strl(const char *data, size_t len) {
    requireRest(6);

    auto newEnd = ::mp_encode_strl(end, len);

    rest -= newEnd - end;
    end = newEnd;

    requireRest(len);

    memcpy(end, data, len);
    rest-=len;
    end+=len;
}

wtf_buffer::~wtf_buffer() {
    if(begin)
        free(begin);
}
